**Contributors**  

* [Ana Schwendler](https://twitter.com/anaschwendler)
* [Brandon Dees](https://twitter.com/brandondees)
* [Eduardo Stuart](https://twitter.com/eduardostuart)
* [Eliezer Salvato](https://medium.com/@eliezersalvato)
* [Iago Cavalcante](https://twitter.com/iagoangelim)
* [Klaus Kazlauskas](https://twitter.com/klauskpm)
* [Leandro Bighetti](https://twitter.com/leandrobighetti)
* [Leandro Camargo](https://twitter.com/leandroico)
* [Nayara Alves (Naahh)](https://twitter.com/_jhorse)
* [Rachel Curioso](https://twitter.com/_rchc)
* [Rafael França](https://twitter.com/rafaelfranca)
* [Rimenes Ribeiro](https://twitter.com/rimenes)
* [William Walron]()

Thank you <3 You're awesome folks!!! <3

